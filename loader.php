<?php
/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 13:49
 */

define(BASEPATH, __DIR__ . DIRECTORY_SEPARATOR);

function __autoload($cname)
{
    if(preg_match('/^.*Controller$/',$cname))
    {
        $dir = "controller";
    }
    if(preg_match('/^.*Model$/',$cname))
    {
        $dir = "model";
    }
    if(preg_match('/^.*View$/',$cname))
    {
        $dir = "view";
    }

    require_once BASEPATH . $dir . DIRECTORY_SEPARATOR . $cname . '.php';
}