<?php

/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 17:04
 */
abstract class View
{
    // To print styles
    protected function Styles()
    {
        $path = "../style/";


        return [
            'style' => "{$path}style.css",
            'bootstrap' => "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css",
        ];
    }

    // To print head of page
    protected function PrintHead($title)
    {
        ?>
        <!DOCTYPE html>
        <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <?php
        foreach($this->Styles() as $name => $path){ ?>
            <link rel="stylesheet" type="text/css" href="<?= $path ?>">
        <?php } ?>
        <title><?= $title ?></title>
    </head>

    <?php
    }

    // To print footer
    protected function PrintFooter()
    {
         echo '</body>
        </html>';
    }
}