<?php

/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 12:44
 */
class AddNewView extends View
{
    private $data;

    // To print html with data
    public function Show($data)
    {
        $this->data = &$data;
        $this->PrintHead($data['title']);
        ?>
        <body>

        <div class="container">
            <a href="index.php" class="btn btn-info" role="button">Show all films</a>
            <?php (!isset($data['saved'])) ? $this->PrintForm() : $this->PrintSaved($data['film_name'])?>
        </div>
        <?php
        $this->PrintFooter();
    }

    // Show Form
    private function PrintForm()
    {
        $this->Notyfication();
        ?>
        <form method="get" action="add.php" >
            <div class="form-group">
                <label for="film_name">Film title:</label>
                <input type="text" name="film_name" id="film_name" class="form-control" placeholder="Title" minlength="1" value="<?= $_GET['film_name']?>">
            </div>
            <div class="form-group">
                <label for="film_year">Released:</label>
                <input type="number" name="film_year" id="film_year" min="1888" max="9999" class="form-control" placeholder="Year" value="<?= $_GET['year']?>">
            </div>
                <input type="submit" class="btn btn-default" value="Add to database">

        </form>
        <?php
    }

    // Show success
    private function PrintSaved($film_name)
    {
        echo '<a href="add.php" class="btn btn-success" role="button">Add new film</a><br />';
        echo "<h2 class=\"bg-success\">Film <strong>{$film_name}</strong> succesfully added!</h2>";
    }

    // Show error
    private function Notyfication()
    {
        if(!empty($_GET['film_year']) && (!is_numeric($_GET['film_year']) || ($_GET['film_year'] < 1887 || $_GET['film_year'] > 9999)))
            echo '<p class="bg-danger">Field <strong>Year</strong> must be numeric value between 1888 and 9999!</p>';


        if($this->data['empty'] !== false)
        {
            foreach($this->data['empty'] as $field => $val)
            {
                echo '<p class="bg-danger">Field <strong>' . $field . '</strong> must be filled!</p>';
            }
        }

    }
}