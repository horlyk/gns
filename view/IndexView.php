<?php

/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 12:44
 */
class IndexView extends View
{
    // To print html with data
    public function Show($data)
    {
        $this->PrintHead($data['title']); ?>

            <body>
                <div class="container">
                    <a href="add.php" class="btn btn-info" role="button">Add new film</a>
                    <table class="all-films-table table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Released</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($data['films'] as $row => $film)
                            {
                                echo "<tr>\n";
                                    echo "<td>{$film->name}</td>\n";
                                    echo "<td>{$film->year}</td>\n";
                                echo "</tr>\n";
                            } ?>
                        </tbody>
                    </table>
                </div>
    <?php
        $this->PrintFooter();
    }
}