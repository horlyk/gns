<?php

/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 12:42
 */
class Model
{
    private $db;

    public function __construct()
    {
        $this->db = DBController::GetDbConnection();
    }

    //get all films from db
    public function GetFilms()
    {
        $link = $this->db->query("SELECT name, year from films WHERE isActive=1");

        while($row = $link->fetchObject())
        {
            $data[] = $row;
        }
        return $data;
    }

    //add new film to db
    public function AddNewFilm()
    {
        $qry = "INSERT INTO films(name, year) VALUES(:name, :year)";
        $stmt = $this->db->prepare($qry);
        $stmt->execute(
            [
                ":name" => $_GET['film_name'],
                ":year" => $_GET['film_year'],
            ]);

        return($stmt->rowCount());
    }
}