<?php

/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 12:29
 */
class DBController
{
    private static $db = null;
    
    private function __construct() {}

    public static function GetDbConnection()
    {
        if(self::$db === null)
        {
            $config = require_once BASEPATH."config.php";

            try
            {
                self::$db = new PDO($config['config'],$config['user'], $config['password']);
            }
            catch(Exception $e)
            {
                echo "<strong>Error! can't connect to db.<br />Message: </strong>{$e->getMessage()}";
                die;
            }
        }
            return self::$db;
    }
}