<?php

/**
 * Created by PhpStorm.
 * User: horlyk
 * Date: 005 05 09.2015
 * Time: 12:41
 */
class Controller
{
    private $model;

    public function __construct($page)
    {
        $this->model = new Model();
        $this->View($page);
    }

    // loads needed view
    private function View($page)
    {
        $view_class = $page . "View";
        $view = new $view_class;


        switch($page)
        {
            case "Index":
                $data = [
                    'title' => "All films",
                    'films' => $this->model->GetFilms(),
                ];
                break;
            case "AddNew":

                $this->ParseGetForSave();
                $data = [
                    'title' => "Add New film",
                    'saved' => $_GET['saved'],
                    'film_name' => $_GET['film_name'],
                    'empty' => $this->GetEmptyFields(),
                ];
                break;
        }

        //print_r($data);
        $view->Show($data);
    }

    // parse get for data saving
    private function ParseGetForSave()
    {
        if($this->Validation() === true)
        {
          if($this->model->AddNewFilm())
          {
              header("Location: http://{$_SERVER['HTTP_HOST']}/add.php?saved=true&film_name={$_GET['film_name']}");
              die;
          }
        }
    }

    // get empty get params for error checking
    private function GetEmptyFields()
    {

        $empty = false;
        if(isset($_GET['film_name']) && empty($_GET['film_name']))
        {
            $empty['Title'] = '';
        }
        if(isset($_GET['film_year']) && empty($_GET['film_year']))
        {
            $empty ['Year'] = '';
        }

        return $empty;
    }

    private function Validation()
    {
        if(!empty($_GET['film_name']) && !empty($_GET['film_year']) && is_numeric($_GET['film_year']) && $_GET['film_year'] > 1887 && $_GET['film_year'] < 9999)
            return true;
        else
            return false;
    }

}